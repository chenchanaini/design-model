package com.junsheng.exten;

public class PersonB  extends PersonA{
    private String name ="B";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected void run() {
        super.run();
    }

}
