package com.junsheng.exten.access.classes.publics;

//import com.junsheng.exten.access.classes.defaults.CarDefault;
/**
 * @Description 访问不了没有修饰符的不同包的类
 * @author shenglei
 * @date 2023/2/22 11:10
 */
public class CarPublic {
//   CarDefault carDefault=new CarDefault();
}
