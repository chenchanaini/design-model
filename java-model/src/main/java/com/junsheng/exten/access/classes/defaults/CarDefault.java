package com.junsheng.exten.access.classes.defaults;

import com.junsheng.exten.access.classes.publics.CarPublic;
/**
 * @Description public修饰的类不同的包可以import
 * @author shenglei
 * @date 2023/2/22 11:10
 */
class CarDefault {
    CarPublic CarPublic =  new CarPublic();
}
