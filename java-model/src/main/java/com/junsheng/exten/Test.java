package com.junsheng.exten;

import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
/**
 * @Description
 * 子类能继承父类的私有成员
 * 子类除了父类的private
 * @author shenglei
 * @date 2023/2/22 11:17
 */
public class Test {
    public static void main(String[] args) throws ClassNotFoundException {
        PersonB personB = new PersonB();
        personB.getName();
        Class<?> classb = Class.forName("com.junsheng.exten.PersonC");
        Class<?>[] interfaces = classb.getInterfaces();
        Type[] genericInterfaces = classb.getGenericInterfaces();
        AnnotatedType[] annotatedInterfaces = classb.getAnnotatedInterfaces();
        TypeVariable<? extends Class<?>>[] typeParameters = classb.getTypeParameters();
        System.out.println(""+ classb.getSuperclass());
        System.out.println(""+classb.getGenericSuperclass());
        System.out.println(""+classb.getInterfaces()[0].getName());
        System.out.println(""+classb.getGenericInterfaces()[0].getTypeName());
        System.out.println(4.0/(12*11*10));
        System.out.println(4.0/(12*11*10));
        Double sum = getInteger(52);
        //顺
        System.out.println(((52*8*4)*2+52*8*8*10)/sum);
        //金
        System.out.println(12/sum);
    }

    private static Double getInteger(Integer m) {
        Double sum =1.0;
        for (int i = 1; i <=m; i++) {
            sum=sum*i;
        }
        return sum;
    }
    //12345678
}
