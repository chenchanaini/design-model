package com.junsheng.exten;

public class PersonA {
    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     * @Description 子类无法访问父类的私有方法
     * @return
     * @author shenglei
     * @date 2023/2/22 10:42
     */
    private void  show(){
        //该方法不能被子类访问,所以不能被override
        System.out.printf("PersonA中的show方法");
    }
    /**
     * @Description 子类可以访问父类的protected方法
     * @return
     * @author shenglei
     * @date 2023/2/22 10:43
     */
    protected void run(){
        //该方法不能被子类访问,所以能被override
        System.out.printf("PersonA中的run方法");
    }

    public final void finalMethod(){
        //该方法能被子类访问,但是不能被override
        System.out.printf("PersonA中的finalMethod方法");
    }

}
