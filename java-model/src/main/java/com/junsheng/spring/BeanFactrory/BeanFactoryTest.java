package com.junsheng.spring.BeanFactrory;

import com.junsheng.spring.RegisterBean.service.UserService;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

public class BeanFactoryTest {
    public static void main(String[] args) {
        DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition.setBeanClass(UserService.class);
        defaultListableBeanFactory.registerBeanDefinition("userservice",beanDefinition);
        UserService bean = defaultListableBeanFactory.getBean(UserService.class);
        System.out.println(bean);
    }
}
