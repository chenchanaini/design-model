package com.junsheng.spring.BeanLifeCycle;

import com.junsheng.spring.RegisterBean.config.SpringConfig2;
import com.junsheng.spring.RegisterBean.service.UserService;
import com.junsheng.spring.RegisterBean.service.UserService2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;

import java.util.Map;
import java.util.Properties;
/*
* 此类是学习如何注册springBean的方式
* 依赖spring-context jar包
*
* */

public class BeanLifeCycleTest {
    public static void main(String[] args) {
        //声明方式
        //注解配置方式1 @Bean注解
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig2.class);
        UserService bean = applicationContext.getBean(UserService.class);
        applicationContext.registerShutdownHook();
        Map<String, Object> systemEnvironment = applicationContext.getEnvironment().getSystemEnvironment();
        for(Map.Entry entry:systemEnvironment.entrySet()){
            System.out.println("systemEnvironment----"+entry.getKey()+":"+entry.getValue());
        }
        Map<String, Object> systemProperties = applicationContext.getEnvironment().getSystemProperties();
        for(Map.Entry entry:systemProperties.entrySet()){
            System.out.println("systemProperties----"+entry.getKey()+":"+entry.getValue());
        }

        MutablePropertySources propertySources = applicationContext.getEnvironment().getPropertySources();
        PropertySource<?> propertySource = propertySources.get("systemProperties");
        Properties source = (Properties) propertySource.getSource();
        for(Object key:source.keySet()){
            System.out.println("systemProperties11----"+key+":"+source.get(key));
        }
//        for(Map.Entry entry:propertySources.entrySet()){
//            System.out.println("systemProperties----"+entry.getKey()+":"+entry.getValue());
//        }
// springboot Binder
//        KnifeProperties config = Binder.get(env).bind("knife", Bindable.of(KnifeProperties.class)).get();

    }
}
