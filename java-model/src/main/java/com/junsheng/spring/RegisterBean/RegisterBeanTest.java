package com.junsheng.spring.RegisterBean;

import com.junsheng.spring.RegisterBean.config.SpringConfig;
import com.junsheng.spring.RegisterBean.config.SpringConfig2;
import com.junsheng.spring.RegisterBean.entity.UserService3;
import com.junsheng.spring.RegisterBean.entity.UserService4;
import com.junsheng.spring.RegisterBean.service.UserService;
import com.junsheng.spring.RegisterBean.service.UserService2;
import com.junsheng.spring.Scanner.MyImportBeanDefinitionRegistrar;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/*
* 此类是学习如何注册springBean的方式
* 依赖spring-context jar包
*
* */

public class RegisterBeanTest {
    public static void main(String[] args) {
//        //声明方式
//        //注解配置方式1 @Bean注解
//        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
//        UserService bean = applicationContext.getBean(UserService.class);
//        System.out.println(bean);
//        //注解配置方式2 包扫描@ComponentScan @Component 注解
//        ApplicationContext applicationContext2 = new AnnotationConfigApplicationContext(SpringConfig2.class);
//        UserService2 bean2 = applicationContext2.getBean(UserService2.class);
//        System.out.println(bean2);
//        //xml配置方式
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring.xml");
        UserService a = classPathXmlApplicationContext.getBean("a", UserService.class);
//
//        //编程方式
//        AnnotationConfigApplicationContext genericApplicationContext = new AnnotationConfigApplicationContext();
//        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
//        beanDefinition.setBeanClass(UserService3.class);
//        genericApplicationContext.registerBeanDefinition("userService3",beanDefinition);
//        genericApplicationContext.refresh();
//        UserService3 bean3 = genericApplicationContext.getBean(UserService3.class);
//        System.out.println(bean3);

        //编程方式
        AnnotationConfigApplicationContext genericApplicationContext1 = new AnnotationConfigApplicationContext();
        AbstractBeanDefinition beanDefinition1 = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition1.setBeanClass(UserServiceFactoryBean.class);
        genericApplicationContext1.registerBeanDefinition("userServiceFactoryBean",beanDefinition1);


        AbstractBeanDefinition beanDefinition2 = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition1.setBeanClass(UserServiceFactoryBean.class);
        genericApplicationContext1.registerBeanDefinition("userServiceFactoryBean2",beanDefinition1);

        genericApplicationContext1.refresh();
        UserService4 bean4= genericApplicationContext1.getBean("userServiceFactoryBean", UserService4.class);
        UserServiceFactoryBean bean5= genericApplicationContext1.getBean("&userServiceFactoryBean", UserServiceFactoryBean.class);
        UserServiceFactoryBean bean6= genericApplicationContext1.getBean("&userServiceFactoryBean2", UserServiceFactoryBean.class);
        System.out.println("bean4-------"+bean4);
        System.out.println("bean5-------"+bean5);
        System.out.println("bean6-------"+bean6);

        //java8 supplier方式注册--beanname不会重复，重复的话后面注册的bean会覆盖之前的bean

        AnnotationConfigApplicationContext genericApplicationContext2 = new AnnotationConfigApplicationContext();
        genericApplicationContext2.registerBean(com.junsheng.spring.RegisterBean.UserService4.class,()->new com.junsheng.spring.RegisterBean.UserService4());
        genericApplicationContext2.registerBean(UserService4.class,()->new UserService4());
        genericApplicationContext2.refresh();
        Object userService4 = genericApplicationContext2.getBean("userService4");
        System.out.println(userService4);


        //ScannerConfig
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(ScannerConfig.class);

        MyImportBeanDefinitionRegistrar bean = annotationConfigApplicationContext.getBean(MyImportBeanDefinitionRegistrar.class);
        System.out.println(bean);
    }
}
