package com.junsheng.spring.RegisterBean.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class UserService implements InitializingBean , DisposableBean, BeanNameAware, ApplicationContextAware, EnvironmentAware, BeanClassLoaderAware, BeanFactoryAware {

    private UserService2 userService2;
    public UserService() {
        System.out.println("实例化方法执行");
    }

    @Autowired
    private void UserService2(UserService2 userService2){
        System.out.println("填充属性");
         this.userService2=userService2;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("初始化方法");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("销毁方法");
    }

    @Override
    public void setBeanName(String s) {
        System.out.println("BeanNameAware执行");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("ApplicationContextAware执行");
    }

    @Override
    public void setEnvironment(Environment environment) {
        System.out.println("EnvironmentAware执行");
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        System.out.println("ClassLoaderAware执行");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("BeanFactoryAware执行");
    }
}
