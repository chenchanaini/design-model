package com.junsheng.spring.RegisterBean.config;

import com.junsheng.spring.RegisterBean.service.UserService;
import org.springframework.context.annotation.Bean;

//@ComponentScan("cn.junsheng.service")
public class SpringConfig {
    @Bean
    private UserService userService(){
        return new UserService();
    }
}
