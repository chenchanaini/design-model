package com.junsheng.spring.RegisterBean;

import com.junsheng.spring.RegisterBean.entity.UserService4;
import org.springframework.beans.factory.FactoryBean;

public class UserServiceFactoryBean implements FactoryBean {
    @Override
    public Object getObject() throws Exception {
        return new UserService4();
    }

    @Override
    public Class<?> getObjectType() {
        return UserService4.class;
    }
}
