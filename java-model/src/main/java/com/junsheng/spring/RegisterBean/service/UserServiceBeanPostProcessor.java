package com.junsheng.spring.RegisterBean.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class UserServiceBeanPostProcessor implements InstantiationAwareBeanPostProcessor {
    /*
    * 如果方法返回的Object不是null 不会执行接下来的生命周期
    * */
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        Object o =null;
        if (beanName.equals("userService")) {
            System.out.println("执行实例化前方法");
//            o = new UserService2();
        }
        return o;
    }
    /*
    * 方法如果返回false将不进行属性填充操作
    * */
    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        if (bean instanceof UserService){
            System.out.println("执行实例化后方法");
        }
        return true;
    }
    /*
    * 这个方法对后续操作没有影响
    * */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof UserService){
            System.out.println("执行初始化前方法");
        }
        return bean;
    }
    /*
     * 这个方法对后续操作没有影响
     * */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof UserService){
            System.out.println("执行初始化后方法");
        }
        return bean;
    }
}
