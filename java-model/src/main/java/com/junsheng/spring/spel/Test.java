package com.junsheng.spring.spel;

import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.HashMap;

public class Test {
    public static void main(String[] args) {
        //表达式的简单使用
        SpelExpressionParser spelExpressionParser = new SpelExpressionParser();
        Expression expression = spelExpressionParser.parseExpression("'hello world'");
        Object value = expression.getValue();
        System.out.println(value);


        Expression expression1 = spelExpressionParser.parseExpression("'hello world'.concat('!')");
        Object value1 = expression1.getValue();
        System.out.println(value1);

        Expression expression2 = spelExpressionParser.parseExpression("'hello world'.getBytes()");
        byte[] value2 = (byte[])expression2.getValue();
        System.out.println(value2);


        Expression expression3 = spelExpressionParser.parseExpression("'hello world'.getBytes().length");
        Integer value3 = (Integer)expression3.getValue();
        System.out.println(value3);


        Expression expression4 = spelExpressionParser.parseExpression("new String('hello world').getBytes().length");
        Integer value4 = (Integer)expression4.getValue();
        System.out.println(value4);



        Investor investor = new Investor("shenglei","yahui",System.currentTimeMillis());
        Expression expression6 = spelExpressionParser.parseExpression("username");
        StandardEvaluationContext standardEvaluationContext = new StandardEvaluationContext(investor);
        Object value6 = expression6.getValue(standardEvaluationContext);
        System.out.println(value6);


        Expression expression7 = spelExpressionParser.parseExpression("time");
        Long value7 = expression7.getValue(investor,Long.class);
        System.out.println(value7);

//        HashMap<String, Object> hashMap = new HashMap<>();
//        hashMap.put("investor",investor );
//        Expression expression5 = spelExpressionParser.parseExpression("investor");
//        Investor value5 = expression5.getValue(hashMap, Investor.class);
//        System.out.println(value5);
        String s = new String("123");
        System.out.println(s.getClass());
        System.out.println(expression7.getClass().getClassLoader());
        System.out.println(expression4.getClass().getClassLoader());
        System.out.println(Thread.class.getClassLoader());
    }
}
