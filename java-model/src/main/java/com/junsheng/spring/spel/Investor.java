package com.junsheng.spring.spel;

public class Investor {
    private String username;
    private String investorName;
    private Long time;

    public Investor(String username, String investorName, Long time) {
        this.username = username;
        this.investorName = investorName;
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
