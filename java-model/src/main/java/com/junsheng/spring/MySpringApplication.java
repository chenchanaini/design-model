package com.junsheng.spring;

import com.junsheng.mapper.UserMapper;
import com.junsheng.spring.annocation.Junsheng;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Junsheng("com.junsheng.mapper")
public class MySpringApplication {
    public static void main(String[] args) {
        //ScannerConfig
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();

        annotationConfigApplicationContext.register(MySpringApplication.class);

        annotationConfigApplicationContext.refresh();

        UserMapper bean = annotationConfigApplicationContext.getBean(UserMapper.class);

        bean.count();
    }
}
