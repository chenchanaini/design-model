package com.junsheng.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompareTest {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(new String []{"ChargeAdDiscountManagerImpl",
                "ChargeAdFeeManagerImpl",
                "ChargeBasicCoreQueryManagerImpl",//没有方法
                "ChargeBasicCoreTranManagerImpl",
                "ChargeBasicPlanCoreQueryManagerImpl",
                "ChargeBasicPlanCoreTranManagerImpl",
                "ChargeChannelSyntheticManageImpl",
                "ChargeChannelSyntheticTaskManagerImpl",
                "ChargeCoreQueryManagerImpl",
                //"ChargeCoreTranManagerImpl",
                "ChargeDiscountInfoQueryManagerImpl",
                "ChargeSyntheticCoreQueryManagerImpl",
                "ChargeSyntheticCoreTranManagerImpl",
                "ChargeSyntheticPlanCoreQueryManagerImpl",
                "ChargeSyntheticPlanCoreTranManagerImpl",
                "ChargeSyntheticSyncManagerImpl",
                "MqClientImpl",
                "OrderStatusManagerImpl"}));
        ArrayList<String> strings_ = new ArrayList<>(Arrays.asList(new String []{"ChargeAdDiscountManagerImplTest",
                "ChargeAdFeeManagerImplTest",
                "ChargeBasicCoreTranManagerImplTest",
                "ChargeBasicPlanCoreQueryManagerImplTest",
                "ChargeBasicPlanCoreTranManagerImplTest",
                "ChargeChannelSyntheticManageImplTest",
                "ChargeChannelSyntheticTaskManagerImplTest",
                "ChargeCoreQueryManagerImplTest",
                "ChargeCoreTranManagerImplPartialMockTest",
                "ChargeDiscountInfoQueryManagerImplTest",
                "ChargeLogGrepImplTest",
                "ChargeSyntheticCoreQueryManagerImplTest",
                "ChargeSyntheticCoreTranManagerImplTest",
                "ChargeSyntheticPlanCoreQueryManagerImplTest",
                "ChargeSyntheticPlanCoreTranManagerImplTest",
                "ChargeSyntheticSyncManagerImplTest"}));

        for (int i = 0; i < strings.size(); i++) {
            strings.set(i,strings.get(i)+"Test");
        }
        System.out.println(strings.size());
        System.out.println(strings_.size());
        strings.removeAll(strings_);

        for (int i = 0; i < strings.size(); i++) {
            System.out.println(strings.get(i));
        }


    }
}
