package com.junsheng.test;

import java.util.ArrayList;
/**
 * @Description 在lamada表达式中，continue只能通过return实现
 * @author shenglei
 * @date 2023/2/27 10:10
 */
public class ReturnTest {
    public static void main(String[] args) {
        testReturn();
        testReturn1();
        testReturn2();
    }
    private static void  testReturn(){
        ArrayList<Object> objects = new ArrayList<>();
        objects.add("12");
        objects.add("34");
        objects.forEach(x->{
            if ("12".equals(x)){
                System.out.println("---------"+x);
                return;
            }
            System.out.println("---------"+x);
        });
    }

    private static void  testReturn1(){
        ArrayList<Object> objects = new ArrayList<>();
        objects.add("12");
        objects.add("34");
        for (int i = 0; i <objects.size(); i++) {
            if ("12".equals(objects.get(i))){
                System.out.println("---------"+objects.get(i));
                return;
            }
            System.out.println("---------"+objects.get(i));
        }

    }
    private static void  testReturn2(){
        ArrayList<Object> objects = new ArrayList<>();
        objects.add("12");
        objects.add("34");
        for (int i = 0; i <objects.size(); i++) {
            if ("12".equals(objects.get(i))){
                System.out.println("---------"+objects.get(i));
                continue;
            }
            System.out.println("---------"+objects.get(i));
        }
    }
}
