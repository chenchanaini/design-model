package com.junsheng.utils;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    public static boolean isTime(String time){
        Pattern pattern = Pattern.compile("[0-9]{2}:[0-9]{2}:[0-9]{2}");
        Matcher matcher = pattern.matcher(time);
        if (!matcher.matches()){
            return false;
        }
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        try {
            format.parse(time);
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
