//package com.junsheng.thread.example;
//
//public class 双线程交替打印0_100_notify {
//    public static void main(String[] args) {
//        int TOTAL = 100;
//        int i =100;
//        Object lock = new Object();
//        Thread thread1 = new Thread(() -> {
//            while (i <= TOTAL) {
//                synchronized (lock) {
//                    if (i % 2 == 1) {
//                        System.out.println("i=" + i++);
//                        lock.notify();
//                        System.out.println("奇数打印完毕，释放锁");
//                    } else {
//                        try {
//                            System.out.println("奇数锁等待");
//
//                            lock.wait();
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        });
//        Thread thread2 = new Thread(() -> {
//            while (i <= TOTAL) {
//                synchronized (lock) {
//                    if (i % 2 == 0) {
//                        System.out.println("i=" + i++);
//                        lock.notify();
//                        System.out.println("偶数打印完毕，释放锁");
//
//                    } else {
//                        try {
//                            System.out.println("偶数锁等待");
//                            lock.wait();
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        });
//        thread1.start();
//        thread2.start();
//}
