package com.junsheng.thread.example;

import java.util.concurrent.atomic.AtomicReference;

public class 双线程交替打印0_100_atomic {
    public static void main(String[] args) {
         AtomicReference<Integer> a = new AtomicReference<>(100);
        Thread thread = new Thread(() -> {
            while (a.get()<=100){
                if (a.get() %2==0){
                    System.out.println(a);
                    a.getAndSet(a.get() - 1);
                }
            }
        });
        Thread thread1 = new Thread(() -> {
            while (a.get()<=100) {
                if (a.get() % 2 == 1) {
                    System.out.println("线程------"+a);
                    a.getAndSet(a.get() - 1);
                }
            }
        });
        thread.start();
        thread1.start();
    }

}
