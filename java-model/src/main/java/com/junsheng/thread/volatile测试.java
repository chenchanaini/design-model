package com.junsheng.thread;

public class volatile测试 {
    static volatile boolean flag =true;
    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            flag=false;
        });
        thread.start();
        while (flag){
            System.out.println("等待收到改动");
        }
        System.out.println("收到改动退出循环");
    }

}
