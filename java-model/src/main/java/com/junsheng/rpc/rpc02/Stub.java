package com.junsheng.rpc.rpc02;

import com.junsheng.rpc.entity.User;
import com.junsheng.rpc.service.UserService;

import java.io.*;
import java.net.Socket;

public class Stub implements UserService {
    @Override
    public User findUserById(Integer id) throws Exception {
        Socket socket = new Socket("127.0.0.1",8888);
        OutputStream outputStream = socket.getOutputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeInt(123);
        outputStream.write(byteArrayOutputStream.toByteArray());

        InputStream inputStream = socket.getInputStream();
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        int ids = dataInputStream.readInt();
        String name = dataInputStream.readUTF();
        User user = new User(name, ids);
        System.out.printf(user.toString());
        dataOutputStream.close();
        dataInputStream.close();
        socket.close();
        return user;
    }
}
