package com.junsheng.rpc.rpc02;

import com.junsheng.rpc.entity.User;
import com.junsheng.rpc.service.UserService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static boolean running =true;

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(8888);
        while (running){
            Socket accept = serverSocket.accept();
            process(accept);
            accept.close();
        }
    }

    private static void process(Socket accept) throws Exception {
        InputStream inputStream = accept.getInputStream();
        OutputStream outputStream = accept.getOutputStream();
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        int id = dataInputStream.readInt();
        UserService userService = new UserServiceImpl();
        User userById = userService.findUserById(id);
        dataOutputStream.writeInt(userById.getId());
        dataOutputStream.writeUTF(userById.getUsername());
        dataOutputStream.flush();
    }
}
