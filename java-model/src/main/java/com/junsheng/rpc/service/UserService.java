package com.junsheng.rpc.service;

import com.junsheng.rpc.entity.User;

public interface UserService {
    User findUserById(Integer id) throws Exception;
}
