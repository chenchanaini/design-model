package com.junsheng.rpc.rpc03;


import com.junsheng.rpc.entity.User;
import com.junsheng.rpc.service.UserService;

public class Client {
    public static void main(String[] args) throws Exception {
        UserService userService = new UserServiceImpl();
        User userById = userService.findUserById(123);
        System.out.printf(userById.toString());
    }
}
