package com.junsheng.rpc.rpc03;

import com.junsheng.rpc.entity.User;
import com.junsheng.rpc.service.UserService;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

public class Stub  {
    InvocationHandler invocationHandler = new InvocationHandler() {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Socket socket = new Socket("127.0.0.1",8888);
            OutputStream outputStream = socket.getOutputStream();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeInt(123);
            outputStream.write(byteArrayOutputStream.toByteArray());

            InputStream inputStream = socket.getInputStream();
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            int ids = dataInputStream.readInt();
            String name = dataInputStream.readUTF();
            User user = new User(name, ids);
            System.out.printf(user.toString());
            dataOutputStream.close();
            dataInputStream.close();
            socket.close();
            return user;
        }
    };
    UserService getStub(){
        Object o = Proxy.newProxyInstance(UserService.class.getClassLoader(), new Class[]{UserService.class}, invocationHandler);
        return (UserService)o;
    }
}
