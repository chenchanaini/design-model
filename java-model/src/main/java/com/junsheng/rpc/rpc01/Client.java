package com.junsheng.rpc.rpc01;


import com.junsheng.rpc.entity.User;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1",8888);
        OutputStream outputStream = socket.getOutputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeInt(123);
        outputStream.write(byteArrayOutputStream.toByteArray());

        InputStream inputStream = socket.getInputStream();
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        int id = dataInputStream.readInt();
        String name = dataInputStream.readUTF();
        User user = new User(name, id);
        System.out.printf(user.toString());
        dataOutputStream.close();
        dataInputStream.close();
        socket.close();
    }
}
