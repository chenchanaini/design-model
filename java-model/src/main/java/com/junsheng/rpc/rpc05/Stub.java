package com.junsheng.rpc.rpc05;

import com.junsheng.rpc.entity.User;
import com.junsheng.rpc.service.UserService;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

public class Stub  {
     InvocationHandler invocationHandler = new InvocationHandler() {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if ("toString".equals(method.getName())){
                Object invoke = method.invoke(proxy, args);
                return invoke;
            }
            Socket socket = new Socket("127.0.0.1",8888);
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeUTF(method.getName());
            Class<?>[] parameterTypes = method.getParameterTypes();
            objectOutputStream.writeObject(parameterTypes);
            objectOutputStream.writeObject(args);
            objectOutputStream.flush();

            InputStream inputStream = socket.getInputStream();
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            int ids = dataInputStream.readInt();
            String name = dataInputStream.readUTF();
            User user = new User(name, ids);
            System.out.printf(user.toString());
            objectOutputStream.close();
            socket.close();
            return user;
        }
    };
   public  UserService getStub(){
        Object o = Proxy.newProxyInstance(UserService.class.getClassLoader(), new Class[]{UserService.class}, invocationHandler);
        return (UserService)o;
    }
}
