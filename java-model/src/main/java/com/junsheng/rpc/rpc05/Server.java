package com.junsheng.rpc.rpc05;

import com.junsheng.rpc.entity.User;
import com.junsheng.rpc.service.UserService;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static boolean running =true;

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(8888);
        while (running){
            Socket accept = serverSocket.accept();
            process(accept);
            accept.close();
        }
    }

    private static void process(Socket accept) throws Exception {
        InputStream inputStream = accept.getInputStream();
        OutputStream outputStream = accept.getOutputStream();
        ObjectInputStream dataInputStream = new ObjectInputStream(inputStream);
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        String methodName = dataInputStream.readUTF();
        Class[] parameterTypes = (Class[])dataInputStream.readObject();
        Object[] args = (Object[])dataInputStream.readObject();
        UserService userService = new UserServiceImpl();
        Method method = userService.getClass().getMethod(methodName, parameterTypes);
        User userById =(User) method.invoke(userService, args);
        dataOutputStream.writeInt(userById.getId());
        dataOutputStream.writeUTF(userById.getUsername());
        dataOutputStream.flush();
    }
}
