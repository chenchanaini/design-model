package com.junsheng.rpc.rpc05;


import com.junsheng.rpc.entity.User;
import com.junsheng.rpc.service.UserService;

public class Client {
    public static void main(String[] args) throws Exception {
        UserService userService = new Stub().getStub();
        User userById = userService.findUserById(123);
        System.out.printf(userById.toString());
    }
}
