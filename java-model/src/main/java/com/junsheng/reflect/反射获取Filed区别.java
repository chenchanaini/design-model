package com.junsheng.reflect;

import com.junsheng.reflect.entity.User;

import java.lang.reflect.Field;

public class 反射获取Filed区别 {
    public static void main(String[] args) {
        Field[] declaredFields = User.class.getDeclaredFields();
        Field[] fields = User.class.getFields();
        System.out.println("----------全部的方法包括继承实现而来的字段，不包含私有字段-----------");
        for (Field field:fields){
            System.out.println(field.getName());
        }
        System.out.println("----------该类本身声明的字段-,包含私有方法----------");
        for (Field field:declaredFields){
            System.out.println(field.getName());
        }
    }
}
