package com.junsheng.reflect;

import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Type;

public class 反射获取interface区别 {
    public static void main(String[] args) {
        Class<?>[] interfaces = UserServiceImpl.class.getInterfaces();//不能获取泛型中的类型字段
        Type[] genericInterfaces = UserServiceImpl.class.getGenericInterfaces();//能获取泛型中的类型字段
        AnnotatedType[] annotatedInterfaces = UserServiceImpl.class.getAnnotatedInterfaces();
        System.out.println("----------全部的方法包括继承实现而来的字段，不包含私有字段-----------");
        for (Class in:interfaces){
            System.out.println(in.getName());
        }
        System.out.println("----------该类本身声明的字段-,包含私有方法----------");
//        for (Field field:declaredFields){
//            System.out.println(field.getName());
//        }
    }
}
