package com.junsheng.reflect.introspector;

import com.junsheng.reflect.entity.User;

import java.beans.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
/**
 * @Description Introspector中的方法获取和反射中的getMethods都是包括父类中的所有public方法
 * @return
 * @author shenglei
 * @date 2023/1/4 16:46
 */
public class Test {
    public static void main(String[] args) throws IntrospectionException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        BeanInfo beanInfo = Introspector.getBeanInfo(User.class);
        Method[] methods = User.class.getMethods();
        PropertyDescriptor username = new PropertyDescriptor("username", User.class);
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor descriptor:propertyDescriptors){
            Method readMethod = descriptor.getReadMethod();
            Method writeMethod = descriptor.getWriteMethod();
            System.out.println(descriptor.getReadMethod());
            System.out.println(descriptor.getName());
        }
        MethodDescriptor[] methodDescriptors = beanInfo.getMethodDescriptors();
        for (MethodDescriptor descriptor:methodDescriptors){
            System.out.println(descriptor.getMethod());
            System.out.println(descriptor.getName());
        }

    }
//    private extraExcel(Workbook workbook,Class classes){
//        Sheet sheet = workbook.getSheetAt(0);
//        int maxRow = sheet.getLastRowNum();
//        Constructor constructor = classes.getConstructor(void.class);
//        Object o = constructor.newInstance();
//        System.out.println("总行数为：" + maxRow);
//        for (int row = 0; row <= maxRow; row++) {
//            //获取最后单元格num，即总单元格数 ***注意：此处从1开始计数***
//            int maxRol = sheet.getRow(row).getLastCellNum();
//            System.out.println("--------第" + row + "行的数据如下--------");
//            for (int rol = 0; rol < maxRol; rol++) {
//                System.out.print(sheet.getRow(row).getCell(rol) + "  ");
//                Field[] declaredFields = classes.getDeclaredFields();
//                for (Field field:declaredFields){
//                    if (field.isAnnotationPresent(Name.class)){
//                        String name = field.getAnnotation(Name.class).name();
//                        if (name.equals(sheet.getRow(row).getCell(rol))){
//                            field.setAccessible(true);
//
//                            Method declaredMethod = classes.getDeclaredMethod("set" + field.getName(), field.getDeclaringClass());
//                            declaredMethod.invoke(o,sheet.getRow(row).getCell(rol+1));
//                        }
//                    }
//                }
//
//            }
//            System.out.println();
//        }
//        return o;
//    }


}
