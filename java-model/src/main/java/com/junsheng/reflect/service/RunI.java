package com.junsheng.reflect.service;

import com.junsheng.spring.annocation.Junsheng;
import org.springframework.beans.factory.annotation.Autowired;
@Junsheng("123")
public interface RunI<T> extends GoI<T> {
    @Autowired
    void run();
}
