package com.junsheng.reflect.service;

import com.junsheng.reflect.annotation.JunshengParent;
import org.springframework.beans.factory.annotation.Autowired;
@JunshengParent("456")
public interface GoI<T> {
    @Autowired
    void go(T t);
}
