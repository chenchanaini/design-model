package com.junsheng.reflect.service;

import com.junsheng.reflect.entity.User;
import org.springframework.beans.factory.annotation.Autowired;

public interface UserServiceI {
    @Autowired
    User findUserById(Integer id);
}
