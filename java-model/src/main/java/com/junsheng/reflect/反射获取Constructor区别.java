package com.junsheng.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.TypeVariable;

public class 反射获取Constructor区别 {
    public static void main(String[] args) {
        Constructor<?>[] declaredConstructors = UserServiceImpl.class.getDeclaredConstructors();
        Constructor<?>[] constructors = UserServiceImpl.class.getConstructors();
        System.out.println("----------全部的方法包括继承实现而来的方法，不包含私有方法-----------");
        for (Constructor method:constructors){
            System.out.println(method.toString());
            TypeVariable typeParameter = method.getTypeParameters()[0];
            System.out.println(typeParameter.getTypeName());
            System.out.println(typeParameter.getBounds()[1].getTypeName());
            Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
            System.out.println(declaredAnnotations[0]);
            System.out.println(method.getAnnotations()[0]);
        }
        System.out.println("----------该类本身声明的方法-,包含私有方法----------");
        for (Constructor method:declaredConstructors){
            System.out.println(method.toString());
            System.out.println(method.getName());
            System.out.println(method.getParameters()[0]);
            System.out.println(method.getParameterTypes()[0]);
            System.out.println(method.getParameterCount());
            int modifiers = method.getModifiers();
            System.out.println(modifiers);
        }

    }
}
