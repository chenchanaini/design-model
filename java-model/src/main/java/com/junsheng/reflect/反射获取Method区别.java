package com.junsheng.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;


public class 反射获取Method区别 {
    public static void main(String[] args) {
        Method[] methods = UserServiceImpl.class.getMethods();
        Method[] methods_ = UserServiceImpl.class.getDeclaredMethods();
        System.out.println("----------全部的方法包括继承实现而来的方法，不包含私有方法-----------");
        for (Method method:methods){
            System.out.println(method.getName());
            Annotation[] annotations = method.getAnnotations();
            for (Annotation annotation:annotations){
                System.out.println(annotation.annotationType().getName());
            }
            Annotation[] annotations_ = method.getDeclaredAnnotations();
            for (Annotation annotation:annotations_){
                System.out.println(annotation.annotationType().getName());
            }
        }
        System.out.println("----------该类本身声明的方法-,包含私有方法----------");
        for (Method method:methods_){
            if (method.getName().equals("findUserByIds")){
                System.out.println(method.getName());
                System.out.println(method.getDeclaredAnnotations()[0]);
            }
        }
    }
}
