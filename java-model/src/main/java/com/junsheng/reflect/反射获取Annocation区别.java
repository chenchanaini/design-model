package com.junsheng.reflect;

import com.junsheng.reflect.entity.AnnocationASon;
import com.junsheng.reflect.entity.AnnocationISon;
import com.junsheng.reflect.entity.AnnocationSon;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class 反射获取Annocation区别 {
    public static void main(String[] args) {

        //作用在类上是否能继承  --类上可以继承,也就是反射的时候可以获取
        Annotation[] declaredAnnotations = AnnocationSon.class.getDeclaredAnnotations();
        Annotation[] annotations = AnnocationSon.class.getAnnotations();
        System.out.println("----------全部的方法包括继承实现而来的字段，不包含私有字段-----------");
        for (Annotation annotation:annotations){
            System.out.println(annotation.toString());
        }
        System.out.println("----------该类本身声明的字段-,包含私有方法----------");
        for (Annotation annotation:declaredAnnotations){
            System.out.println(annotation.toString());
        }

        //作用在接口上是否能继承  --接口中无法继承,也就是反射的时候获取不到
        Annotation[] declaredAnnotations_ = AnnocationISon.class.getDeclaredAnnotations();
        Annotation[] annotations_ = AnnocationISon.class.getAnnotations();
        Object o = Proxy.newProxyInstance(AnnocationISon.class.getClassLoader(), AnnocationISon.class.getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return null;
            }
        });
        Annotation[] annotations1 = o.getClass().getAnnotations();
        System.out.println("------接口----全部的方法包括继承实现而来的字段，不包含私有字段-----------");
        for (Annotation annotation:annotations_){
            System.out.println(annotation.toString());
        }
        System.out.println("-------接口----该类本身声明的字段-,包含私有方法----------");
        for (Annotation annotation:declaredAnnotations_){
            System.out.println(annotation.toString());
        }
        System.out.println("-------接口的代理类----该类本身声明的字段-,包含私有方法----------");
        for (Annotation annotation:annotations1){
            System.out.println(annotation.toString());
        }
        //作用在抽象类上是否能继承  -类上可以继承,也就是反射的时候可以获取
        Annotation[] declaredAnnotations__ = AnnocationASon.class.getDeclaredAnnotations();
        Annotation[] annotations__ = AnnocationASon.class.getAnnotations();
        System.out.println("------抽象类----全部的方法包括继承实现而来的字段，不包含私有字段-----------");
        for (Annotation annotation:annotations__){
            System.out.println(annotation.toString());
        }
        System.out.println("-------抽象类----该类本身声明的字段-,包含私有方法----------");
        for (Annotation annotation:declaredAnnotations__){
            System.out.println(annotation.toString());
        }


    }
}
