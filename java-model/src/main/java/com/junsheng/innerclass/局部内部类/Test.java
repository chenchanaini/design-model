package com.junsheng.innerclass.局部内部类;

import com.junsheng.innerclass.匿名内部类.A;

public class Test {

    public static void main(String[] args) {
        //new出接口或者实现类
        com.junsheng.innerclass.匿名内部类.A a = new A() {
            //实现接口里未实现的方法
            public void B() {
                System.out.println("匿名内部类");
            }
        };
        a.A();
        a.B();
    }
}