package com.junsheng.innerclass.局部内部类;
/**
 * 局部内部类
 * 作用范围：方法体内
 * 编译过程会自动生成单独的class文件，并且该class有包含所在类实例的有参构造方法。
 * */
import com.junsheng.innerclass.匿名内部类.B;
public abstract class A implements B {

    public void A(){

        class C{
            private String c;
        };
        C c = new C();
        c.c ="3";
        System.out.println(c.c);
    }
}