package com.junsheng.innerclass.成员内部类;

/**
 * 成员内部类
 * 作用范围:类,抽象类，接口内部
 * 编译过程外部类的实例作为其中一个有参构造方法
 * 创建类的时候先创建外部类
 * */
public class Test {
    public static void main(String[] args) {
        Out out = new Out();
        Out.In in = out.new In();
        in.sayHello();
    }
}
