package com.junsheng.innerclass.成员内部类;

public class Out {
   private  String a = "成员外部类的成员变量";
   private  void show(){
        System.out.println("成员外部类的成员方法");
    }
   public  class In{
       public void sayHello(){
           System.out.println(a);
           show();
       }
   }
}
