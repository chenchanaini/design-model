package com.junsheng.innerclass.静态内部类;
/**
 * 静态内部类
 * 作用范围:类,抽象类，接口内部
 * 编译过程通过直接通过类名直接访问变量和方法
 * 创建类的时候直接类名.就能创建
 * */
public class Test {
    public static void main(String[] args) {
        Out.In in = new Out.In();
        in.sayHello();
    }
}
