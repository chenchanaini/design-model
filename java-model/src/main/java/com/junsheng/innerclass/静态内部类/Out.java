package com.junsheng.innerclass.静态内部类;

public class Out {
   private static String a = "成员内部类的成员变量";
   private static void show(){
        System.out.println("成员内部类的成员方法");
    }
   public static class In{
       public void sayHello(){
           System.out.println(a);
            show();
       }
   }
}
