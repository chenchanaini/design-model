package com.junsheng.innerclass.匿名内部类;
/**
 * 匿名内部类
 * 作用范围：类，抽象类，接口
 * 编译过程通过继承，或者实现等方式生成单独的类class文件
 * 只有是接口并且只有一个方法时才能使用lambada表达式进行定义
 * */
public class Test {

    public static void main(String[] args) {
        //1.匿名内部类作用于普通类
            Out out = new Out(){
                @Override
                public void sayHello(){
                    System.out.println("匿名内部类作用于普通类");
                }
            };
            out.sayHello();
        //2.匿名内部类作用于抽象类
        A a = new A() {
            public void B() {
                System.out.println("匿名内部类作用于抽象类");
            }
        };
        a.A();
        a.B();
        //3.匿名内部类作用于接口
       B b =()->{
           System.out.println("匿名内部类作用于接口");
       };
        B b1 = new B() {
            @Override
            public void B() {
                System.out.println("匿名内部类作用于接口");
            }
        };
        System.out.println(out);
        System.out.println(a);
        System.out.println(b);
        System.out.println(b1);
    }
}