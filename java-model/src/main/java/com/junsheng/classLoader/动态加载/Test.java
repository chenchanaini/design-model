package com.junsheng.classLoader.动态加载;

import com.junsheng.classLoader.动态加载.interfac.Register;

import java.lang.reflect.InvocationTargetException;
//15-个人
//6-订金
//5-爸妈
//7-礼金
//10-婵
//-- 43（总）
//20-借 -4w -16w买车

//酒席 1.2w(饭菜)+0.9w(婚车司仪)+2w(烟酒礼物)+0.5(响)=4.6w
public class Test {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Class<?> aClass = Class.forName("com.junsheng.classLoader.动态加载.interfac.MyRegister");
        Object o = aClass.getDeclaredConstructor().newInstance();
        if (o instanceof Register){
            Register register = (Register)o;
            System.out.println( register.register("127.1.1.1","8080"));
        }
//        System.out.println(4*0.1==0.4);
//        System.out.println(3*0.1);
//        System.out.println(0.3);
//        System.out.println(4*0.1);
//        System.out.println(0.4);
//        原公司个人成本： 养老 10000*16% =1600 医保 10000*8% =800 公积金 10000*7%=700
//            现在的公司： 养老 6000*16% =960  医保  6000*8% =480 公积金 6000*7%=420
//            差额 ： 养老 640 医保 320 公积金 280
        System.out.println(480/0.07);
    }
}
