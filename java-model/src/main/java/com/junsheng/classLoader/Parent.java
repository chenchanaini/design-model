package com.junsheng.classLoader;

/**
 * Description: 编译报错
 * 类加载器的过程
 * 1.加载 --》双亲委派模型
 * 2.连接 （校验class 准备阶段--静态变量默认值  解析）-->
 * 3.初始化（父类（静态代码块，静态变量初始化）子类（静态代码块，静态变量初始化））
 * 4.使用
 * 5.卸载
 * 例子:  int 准备阶段 i=0   初始化阶段i=2（静态代码块）,i=1（静态变量初始化）
 * 对象new  成员变量初始化,构造方法
 *
 */
public class Parent {
    static {
        System.out.println("执行父类静态代码块");
        i1 = 2;
        // 编译报错Illegal forward reference
    }
    public static int i1 =3;
    public String a1 ="3";
    public Parent(){
        System.out.println("父类构造方法执行------");
        a1="2";
    }
}