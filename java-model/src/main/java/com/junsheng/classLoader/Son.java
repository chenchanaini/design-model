package com.junsheng.classLoader;

/**
 * Description: 编译报错
 * 类加载器的过程
 * 加载   连接 （校验class 准备阶段--静态变量默认值  解析）  初始化（静态代码块，静态变量初始化，成员变量初始化,构造方法）       使用   卸载
 * int 准备阶段 i=0   初始化阶段i=2（静态代码块）,i=1（静态变量初始化）
 * @author TongWei.Chen 2021-01-08 17:37:44
 */
public class Son extends Parent {
    static {
        // 编译没报错
        System.out.println("执行子类静态代码块");
        i = 2;
    }
    public static int i =1;
    public String a ="1";
    public Son(){
        System.out.println("子类构造方法执行------");
        a="2";
    }
}