package com.junsheng.classLoader;

import org.springframework.context.ApplicationContext;
/**
 * Description: 编译报错
 * 类加载器的过程
 * 1.加载 --》双亲委派模型
 * 2.连接 （校验class 准备阶段--静态变量默认值  解析）-->
 * 3.初始化（父类（静态代码块，静态变量初始化）子类（静态代码块，静态变量初始化））
 * 4.使用
 * 5.卸载
 * 例子:  int 准备阶段 i=0   初始化阶段i=2（静态代码块）,i=1（静态变量初始化）
 * 对象new  成员变量初始化,构造方法
 */
public class Test {
    public static void main(String[] args) {
        Son son = new Son();
        System.out.println(son.i);
        System.out.println(son.a);
        System.out.println(son.a1);
        System.out.println(son.i1);
        System.out.println(ApplicationContext.class.getClassLoader());
    }
}