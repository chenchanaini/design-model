package com.junsheng.设计模式.创建型.factory;

public class Apple implements Fruit{
    @Override
    public void growup() {
        System.out.println("我是一个苹果");
    }
}
