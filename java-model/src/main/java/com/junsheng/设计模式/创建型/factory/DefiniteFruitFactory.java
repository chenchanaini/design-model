package com.junsheng.设计模式.创建型.factory;

public class DefiniteFruitFactory implements FruitFactory{
    @Override
    public Fruit getFruit() {
        return new Apple();
    }
}
