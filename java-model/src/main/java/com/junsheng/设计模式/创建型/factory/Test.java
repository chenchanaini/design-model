package com.junsheng.设计模式.创建型.factory;

public class Test {
    public static void main(String[] args) {
        FruitFactory factory = new DefiniteFruitFactory();
        Fruit fruit = factory.getFruit();
        fruit.growup();
    }
}
