package com.junsheng.设计模式.创建型.single;

import com.junsheng.设计模式.创建型.single.hungry.HungryInstance;
import com.junsheng.设计模式.创建型.single.lazy.LazyInstance;

public class Test {
    public static void main(String[] args) {

        Thread thread = new Thread(() -> {
            while (true){
                LazyInstance instance = LazyInstance.getInstance();
                HungryInstance instance1 = HungryInstance.getInstance();
                System.out.println("-----"+instance);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("-----"+instance1);
            }
        });
        Thread thread1 = new Thread(() -> {
            while (true){
                LazyInstance instance = LazyInstance.getInstance();
                HungryInstance instance1 = HungryInstance.getInstance();
                System.out.println("-----"+instance);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("-----"+instance1);
            }
        });
        thread.start();
        thread1.start();
    }
}
