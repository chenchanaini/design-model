package com.junsheng.设计模式.创建型.builder;

public abstract class Builder {
    protected  Product product = new Product();

    public abstract Builder buildPartA(String A);
    public abstract Builder buildPartB(String B);
    public abstract Builder buildPartC(String C);
    public Product build(){
        return product;
    }
}
