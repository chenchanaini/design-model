package com.junsheng.设计模式.创建型.factory;

public interface FruitFactory {
    Fruit getFruit();
}
