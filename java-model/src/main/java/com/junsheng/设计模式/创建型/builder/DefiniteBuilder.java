package com.junsheng.设计模式.创建型.builder;

public  class DefiniteBuilder extends Builder {

    @Override
    public Builder buildPartA(String A) {
        System.out.println("正在组装A部件");
        this.product.setPartA(A);
        return this;
    }

    @Override
    public Builder buildPartB(String B) {
        System.out.println("正在组装B部件");
        this.product.setPartB(B);
        return this;
    }

    @Override
    public Builder buildPartC(String C) {
        System.out.println("正在组装C部件");
        this.product.setPartC(C);
        return this;
    }
}
