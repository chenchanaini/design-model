package com.junsheng.设计模式.创建型.builder;

public class Test {
    public static void main(String[] args) {
        Builder builder = new DefiniteBuilder();
        Director director = new Director(builder);
        Product product = director.build();
        product.show();
        User zhangsan = new User.Builder().username("zhangsan").password("123").builder();
        System.out.println(zhangsan);
    }
}
