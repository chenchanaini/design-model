package com.junsheng.设计模式.创建型.builder;

public  class Director {
    private Builder builder ;
    public Director( Builder builder){
        this.builder =builder;
    }
    public Product build(){
        builder.buildPartA("A").buildPartB("B").buildPartC("C");
        return builder.build();
    }
}
