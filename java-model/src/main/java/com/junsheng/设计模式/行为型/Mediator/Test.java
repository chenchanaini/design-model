package com.junsheng.设计模式.行为型.Mediator;

public class Test {
    public static void main(String[] args) {
        Mediator mediator = new MediatorImpl();
        CollegeImpl college = new CollegeImpl();
        college.message="我的手机号码换了";
        mediator.register(college);
        mediator.register(new CollegeImpl());
        mediator.register(new CollegeImpl());
        mediator.reply(college);
    }
}
