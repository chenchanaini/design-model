package com.junsheng.设计模式.行为型.Mediator;

public  class MediatorImpl extends Mediator{

    @Override
    void register(College college) {
    if (!collegeList.contains(college)){
        collegeList.add(college);
    }
    }

    @Override
    void reply(College college) {
        System.out.println("发送消息"+college.send());
        collegeList.stream().filter(x->!(college==x)).forEach(x->x.receive(college.send()));
    }
}
