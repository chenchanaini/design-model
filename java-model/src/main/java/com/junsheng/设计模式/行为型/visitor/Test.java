package com.junsheng.设计模式.行为型.visitor;

public class Test {
    public static void main(String[] args) {
        ObjectStructure objectStructure = new ObjectStructure();
        Element concreteElementA = new ConcreteElementA();
        Element concreteElementB = new ConcreteElementB();
        objectStructure.add(concreteElementA);
        objectStructure.add(concreteElementB);
        Visitor concreteVisitorA = new ConcreteVisitorA();
        Visitor concreteVisitorB = new ConcreteVisitorA();
        objectStructure.accept(concreteVisitorA);
        objectStructure.accept(concreteVisitorB);
    }
}
