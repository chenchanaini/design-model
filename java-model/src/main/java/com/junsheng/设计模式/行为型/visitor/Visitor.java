package com.junsheng.设计模式.行为型.visitor;

interface Visitor {
    void visit(ConcreteElementA element);
    void visit(ConcreteElementB element);
}