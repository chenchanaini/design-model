package com.junsheng.设计模式.行为型.responsibilitychain;
// 责任链模式
public class Test {
    public static void main(String[] args) {
        HandleRequest handleRequest = new HandleRequest1();
        handleRequest.setNext(new HandleRequest2());
        handleRequest.HandleRequest("2");
    }
}
