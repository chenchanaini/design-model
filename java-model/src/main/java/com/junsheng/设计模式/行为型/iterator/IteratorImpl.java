package com.junsheng.设计模式.行为型.iterator;

import java.util.List;

public class IteratorImpl implements Iterator {
    private List<Object> list = null;
    private int index = -1;

    public IteratorImpl(List<Object> list) {
        this.list = list;
    }

    @Override
    public Object first() {
        index = 0;
        return list.get(0);
    }

    @Override
    public Object next() {
        Object obj = null;
        if (this.hasNext()) {
            obj = list.get(++index);
        }
        return obj;
    }

    @Override
    public boolean hasNext() {
        if (index < list.size() - 1) {
            return true;
        } else {
            return false;
        }
    }
}
