package com.junsheng.设计模式.行为型.state;

public class Context {
    private State state;

    public Context() {
        this.state = new StateImpl();
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
    void handle(){
        state.Handle(this);
    }
}
