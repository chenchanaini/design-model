package com.junsheng.设计模式.行为型.iterator;

public interface Iterator {
    Object first();
    Object next();
    boolean hasNext();
}
