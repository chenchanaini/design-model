package com.junsheng.设计模式.行为型.iterator;

public class Test {
    public static void main(String[] args) {
        ConcreteAggregate concreteAggregate = new ConcreteAggregate();
        concreteAggregate.add("123");
        concreteAggregate.add("456");
        concreteAggregate.add("789");
        Iterator iterator = concreteAggregate.getIterator();

        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        Object first = iterator.first();
        System.out.println(first);
    }
}
