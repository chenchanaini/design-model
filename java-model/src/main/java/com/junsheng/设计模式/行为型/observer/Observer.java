package com.junsheng.设计模式.行为型.observer;

public abstract class Observer {
    abstract void response();
}
