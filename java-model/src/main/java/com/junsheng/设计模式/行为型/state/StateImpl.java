package com.junsheng.设计模式.行为型.state;

public class StateImpl implements State {
    @Override
    public void Handle(Context context) {
        System.out.println("StateImpl");
        context.setState(new StateImpl1());
    }
}
