package com.junsheng.设计模式.行为型.state;

public class Test {
    public static void main(String[] args) {
        Context context = new Context();
        context.handle();
        context.handle();
        context.handle();
        context.handle();
    }
}
