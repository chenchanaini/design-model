package com.junsheng.设计模式.行为型.expression;

import java.util.HashSet;
import java.util.Set;

public class TerminalExpression implements Expression {
    private Set<String> set =  new HashSet();

    public TerminalExpression(String[] data) {
        for (int i = 0; i < data.length; i++) set.add(data[i]);
    }
    @Override
    public boolean evaluate(String expression){
        if (set.contains(expression)){
            return true;
        }else {
            return false;
        }
    };
}
