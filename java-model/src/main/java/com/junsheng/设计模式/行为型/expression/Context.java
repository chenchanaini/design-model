package com.junsheng.设计模式.行为型.expression;

public class Context {
   String [] persons = {"老人","小孩","军人"};
   String [] address = {"安徽","河南"};
   Expression cityPerson;

    public Context() {
        Expression city = new TerminalExpression(address);
        Expression person = new TerminalExpression(persons);
        cityPerson = new AndExpression(city, person);
    }
    boolean evaluate(String expression){
       return cityPerson.evaluate(expression);
    }
}
