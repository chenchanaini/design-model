package com.junsheng.设计模式.行为型.observer;

public  class SubjectImpl extends Subject{

    @Override
    void notifys() {
        System.out.println("提醒所有的观察者");
        this.observerList.forEach(x->x.response());
    }
}
