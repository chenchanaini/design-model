package com.junsheng.设计模式.行为型.visitor;

public class ConcreteVisitorA implements Visitor{
    @Override
    public void visit(ConcreteElementA element) {
        System.out.println("访问"+element.operation());
    }

    @Override
    public void visit(ConcreteElementB element) {
        System.out.println("访问"+element.operation());
    }
}
