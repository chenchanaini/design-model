package com.junsheng.设计模式.行为型.state;

public interface State {
    void Handle(Context context);
}
