package com.junsheng.设计模式.行为型.iterator;

interface Aggregate {
    public void add(Object obj);
    public void remove(Object obj);
    public Iterator getIterator();
}