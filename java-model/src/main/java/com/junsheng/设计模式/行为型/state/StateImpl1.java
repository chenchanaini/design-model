package com.junsheng.设计模式.行为型.state;

public class StateImpl1 implements State {
    @Override
    public void Handle(Context context) {
        System.out.println("StateImpl1");
        context.setState(new StateImpl());
    }
}
