package com.junsheng.设计模式.行为型.expression;

public interface Expression {
    boolean evaluate(String expression);
}
