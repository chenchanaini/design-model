package com.junsheng.设计模式.行为型.strategy;

public class Kitchen {
    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void Cook(){
        strategy.strategy();
    }
}
