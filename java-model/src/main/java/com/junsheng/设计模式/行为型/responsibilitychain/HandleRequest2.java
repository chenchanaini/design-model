package com.junsheng.设计模式.行为型.responsibilitychain;

public  class HandleRequest2 extends HandleRequest{

    @Override
    public void HandleRequest(String request) {
        if ("2".equals(request)){
            System.out.println("处理器2 处理了该请求");
        }else {
            if (this.next!=null){
                next.HandleRequest(request);
            }else {
                System.out.println("责任链结束,没能处理该请求");
            }
        }

    }
}
