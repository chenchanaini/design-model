package com.junsheng.设计模式.行为型.command;

public interface Command {
  void execute();
}
