package com.junsheng.设计模式.行为型.Mediator;

public abstract class College {
    String message;
    Mediator mediator;
    abstract void receive(String message);
    abstract String send();
}
