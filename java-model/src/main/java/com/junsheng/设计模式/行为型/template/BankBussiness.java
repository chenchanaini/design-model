package com.junsheng.设计模式.行为型.template;

public abstract class BankBussiness {
    public void template(){
        //1.取号
        quhao();
        //2.排队
        paidui();
        //3.办业务
        yewu();
        //领积分的钩子函数
        if(HookMethod1()){
            System.out.println("领积分");
        }
        //4.评价
        pingjia();
    }

    public boolean HookMethod1() {
        return true;
    }

    private void quhao(){
        //1.取号
        System.out.println("取号");
    }

    private void paidui(){
        //1.取号
        System.out.println("排队");
    }

    abstract void yewu();

    private void pingjia(){
        //4.评价
        System.out.println("评价");
    }
}
