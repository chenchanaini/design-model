package com.junsheng.设计模式.行为型.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject {
    List<Observer> observerList = new ArrayList<>();

    void add(Observer observer){
        observerList.add(observer);
    }
    void remove (Observer observer){
        observerList.remove(observer);
    }
    abstract void notifys();
}
