package com.junsheng.设计模式.行为型.strategy;

public interface Strategy {
    void strategy();
}
