package com.junsheng.设计模式.行为型.visitor;

public class ConcreteElementA implements Element{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
    public String operation(){
        return "具体元素A的操作";
    }
}
