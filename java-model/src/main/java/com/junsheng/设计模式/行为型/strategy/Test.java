package com.junsheng.设计模式.行为型.strategy;
// 策略模式
public class Test {
    public static void main(String[] args) {
        Kitchen kitchen = new Kitchen();
        kitchen.setStrategy(new Strategy1());
        kitchen.Cook();
        kitchen.setStrategy(new Strategy2());
        kitchen.Cook();
    }
}
