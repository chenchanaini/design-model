package com.junsheng.设计模式.行为型.responsibilitychain;

public abstract class HandleRequest {
    protected HandleRequest next;
    public abstract void HandleRequest (String request);

    public HandleRequest getNext() {
        return next;
    }

    public void setNext(HandleRequest next) {
        this.next = next;
    }
}
