package com.junsheng.设计模式.行为型.Mediator;

import java.util.ArrayList;
import java.util.List;

public abstract class Mediator {
    List<College> collegeList = new ArrayList<>();
    abstract void register(College college);
    abstract void  reply(College college);
}
