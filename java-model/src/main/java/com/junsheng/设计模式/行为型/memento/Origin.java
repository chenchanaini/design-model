package com.junsheng.设计模式.行为型.memento;

public class Origin {
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    public Memento createMemento(){
        return new Memento(state);
    }

    public void saveMemento(Memento memento){
        state = memento.getState();
    }
}
