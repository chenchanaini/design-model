package com.junsheng.设计模式.行为型.command;

public  class Command1 implements Command {

    private Reciver reciver;
    public Command1() {
        this.reciver = new Reciver();
    }

    @Override
    public void execute() {
        reciver.action();
    }
}
