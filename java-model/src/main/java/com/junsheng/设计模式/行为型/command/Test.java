package com.junsheng.设计模式.行为型.command;
/**
 * 命令模式
 * */
public class Test {
    public static void main(String[] args) {
        Command command1 = new Command1();
        Invoker invoker = new Invoker(command1);
        invoker.send();
    }
}
