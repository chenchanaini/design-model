package com.junsheng.设计模式.行为型.expression;

public class Test {
    public static void main(String[] args) {
        Context context = new Context();
        boolean flag = context.evaluate("上海的儿童");
        System.out.println(flag);
        boolean flag1 = context.evaluate("河南的军人");
        System.out.println(flag1);
    }
}
