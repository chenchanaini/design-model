package com.junsheng.设计模式.行为型.memento;

public class Memento {
    private String state;
    public Memento(String state) {
        this.state =state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
