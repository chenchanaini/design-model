package com.junsheng.设计模式.行为型.template;
/**
 * 模板方法，试用于有些方法是固定的，有一些需要动态改变的方式,还可以通过钩子函数控制父类的执行流程
 * */
public class Test {
    public static void main(String[] args) {
        BankBussiness bankBussiness = new CqBankBussiness();
        BankBussiness qqBankBussiness = new QqBankBussiness();
        bankBussiness.template();
        System.out.println("----------------");
        qqBankBussiness.template();
    }
}
