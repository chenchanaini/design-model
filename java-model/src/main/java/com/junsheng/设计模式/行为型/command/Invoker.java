package com.junsheng.设计模式.行为型.command;
/**
 * 调用者设置并发起命令
 * */
public class Invoker {
    public Invoker(Command command) {
        this.command = command;
    }

    private Command command;

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
   void send(){
        command.execute();
   }
}
