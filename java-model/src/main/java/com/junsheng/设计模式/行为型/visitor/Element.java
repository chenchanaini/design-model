package com.junsheng.设计模式.行为型.visitor;

public interface Element {
    void accept(Visitor visitor);
}
