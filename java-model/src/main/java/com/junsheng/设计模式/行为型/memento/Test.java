package com.junsheng.设计模式.行为型.memento;

public class Test {
    public static void main(String[] args) {
        Manage manage = new Manage();
        Origin origin = new Origin();
        origin.setState("s0");
        System.out.println("原始的状态是"+origin.getState());
        manage.setMemento(origin.createMemento());//存储状态
        origin.setState("s1");
        System.out.println("修改的状态是"+origin.getState());
        //恢复状态
        origin.saveMemento(manage.getMemento());
        System.out.println("恢复的状态是"+origin.getState());



    }
}
