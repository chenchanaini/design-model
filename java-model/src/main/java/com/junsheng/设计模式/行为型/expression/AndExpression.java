package com.junsheng.设计模式.行为型.expression;

public class AndExpression implements Expression {
    private Expression person=  null;
    private Expression address = null;

    public AndExpression(Expression address, Expression person) {
        this.person = person;
        this.address = address;
    }

    @Override
   public boolean evaluate(String expression){
        String s[] = expression.split("的");
        return address.evaluate(s[0]) && person.evaluate(s[1]);
    };
}
