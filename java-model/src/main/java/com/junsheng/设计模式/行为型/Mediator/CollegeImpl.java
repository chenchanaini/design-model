package com.junsheng.设计模式.行为型.Mediator;

public  class CollegeImpl extends College{

    @Override
    void receive(String message) {
        System.out.println("接收到消息"+message);
    }

    @Override
    String send() {
        return message;
    }
}
