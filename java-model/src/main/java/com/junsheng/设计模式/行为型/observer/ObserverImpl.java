package com.junsheng.设计模式.行为型.observer;

public  class ObserverImpl extends Observer {
    @Override
    void response() {
        System.out.println("收到观察者改变通知");
    }
}
