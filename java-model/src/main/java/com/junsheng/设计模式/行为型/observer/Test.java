package com.junsheng.设计模式.行为型.observer;

public class Test {
    public static void main(String[] args) {
        Subject subject = new SubjectImpl();
        Observer observer = new ObserverImpl();
        Observer observer1 = new ObserverImpl();
        subject.add(observer);
        subject.add(observer1);

        subject.notifys();
    }
}
