package com.junsheng.设计模式.结构型.adapter;

public class Adaptor extends Adaptee implements Target   {
    @Override
    public void operation() {
        super.method();
    }
}
