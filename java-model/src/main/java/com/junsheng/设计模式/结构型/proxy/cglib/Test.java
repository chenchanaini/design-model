package com.junsheng.设计模式.结构型.proxy.cglib;

import com.junsheng.设计模式.结构型.proxy.Principal;
import com.junsheng.设计模式.结构型.proxy.User;
import net.sf.cglib.proxy.Enhancer;

import java.util.Random;

public class Test {


    public static void main(String[] args) {
        Principal user = new User();
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(user.getClass());
        enhancer.setCallback(new CglibProxy(user));
        Principal cglibProxy = (Principal) enhancer.create();
        cglibProxy.getUsername();
        System.out.println(user.getClass());
        System.out.println(cglibProxy.getClass());


//  最简单的cglib的实现方式
//        Enhancer enhancer1 = new Enhancer();
//        enhancer1.setSuperclass(user.getClass());
//        enhancer1.setCallback(new MethodInterceptor() {
//            @Override
//            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
//                System.out.println("执行代理逻辑-----------");
//                return null;
//            }
//        });
//        Principal cglibProxy1 = (Principal) enhancer.create();

    }
}
