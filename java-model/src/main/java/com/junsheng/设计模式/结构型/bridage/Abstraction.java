package com.junsheng.设计模式.结构型.bridage;

public abstract class Abstraction  {
    public Abstraction(Implementor implementor) {
        this.implementor = implementor;
    }

    protected Implementor implementor;
    abstract void operation();
}
