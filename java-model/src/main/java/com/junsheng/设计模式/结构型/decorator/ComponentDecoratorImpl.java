package com.junsheng.设计模式.结构型.decorator;

public  class ComponentDecoratorImpl extends ComponentDecorator {

    public ComponentDecoratorImpl(Component component) {
        super(component);
    }

    @Override
    public void operation(){
        super.operation();
        addedFunction();
    }

    private void addedFunction() {
        System.out.println("添加功能");
    }

    ;
}
