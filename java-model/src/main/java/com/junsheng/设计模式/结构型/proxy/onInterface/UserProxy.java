package com.junsheng.设计模式.结构型.proxy.onInterface;

import com.junsheng.设计模式.结构型.proxy.Principal;

public class UserProxy implements Principal {
    private Principal principal;

    public UserProxy(Principal principal) {
        this.principal = principal;
    }

    @Override
    public String getUsername() {
        String username = principal.getUsername();
        username ="zhangsan";
        return username;
    }
}
