package com.junsheng.设计模式.结构型.adapter;
//适配器模式，适合不兼容的类和接口Adaptee,Target
public class Test {
    public static void main(String[] args) {
        Target target = new Adaptor();
        target.operation();
    }
}
