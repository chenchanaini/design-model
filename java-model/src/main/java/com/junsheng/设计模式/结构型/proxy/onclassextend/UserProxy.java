package com.junsheng.设计模式.结构型.proxy.onclassextend;

import com.junsheng.设计模式.结构型.proxy.Principal;
import com.junsheng.设计模式.结构型.proxy.User;

public class UserProxy extends User implements Principal {
    @Override
    public String getUsername() {
        String username = super.getUsername();
        username = "zhangsan";
        return username;
    }
}
