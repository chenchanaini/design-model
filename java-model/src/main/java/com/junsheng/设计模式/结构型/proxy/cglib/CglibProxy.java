package com.junsheng.设计模式.结构型.proxy.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

//Cglib动态代理，实现MethodInterceptor接口
public class CglibProxy implements MethodInterceptor {
    public CglibProxy(Object target) {
        this.target = target;
    }

    private Object target;//需要代理的目标对象

    //重写拦截方法
    public Object intercept(Object obj, Method method, Object[] arr, MethodProxy proxy) throws Throwable {
        System.out.println("Cglib动态代理，监听开始！");
        Object invoke = method.invoke(target, arr);//方法执行，参数：target 目标对象 arr参数数组
        System.out.println("Cglib动态代理"+invoke);
        System.out.println("Cglib动态代理，监听结束！");
        return invoke;
    }

}