package com.junsheng.设计模式.结构型.proxy.classload;

import com.junsheng.设计模式.结构型.proxy.Principal;
import com.junsheng.设计模式.结构型.proxy.User;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Principal user = new User();
        Object o = Proxy.newProxyInstance(user);
        System.out.println(o.getClass());
    }
}
