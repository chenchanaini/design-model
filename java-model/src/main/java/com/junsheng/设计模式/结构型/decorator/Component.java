package com.junsheng.设计模式.结构型.decorator;

public interface Component {
    void operation();
}
