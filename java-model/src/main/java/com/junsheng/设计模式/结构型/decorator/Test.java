package com.junsheng.设计模式.结构型.decorator;
//ComponentImpl传入不同的装饰者会具备不同的能力增强
//装饰器模式就是把对象的属性转移到新的对象上，然后调用新的对象里的新方法
public class Test {
    public static void main(String[] args) {
        ComponentImpl component = new ComponentImpl();
        ComponentDecorator componentDecorator = new ComponentDecoratorImpl(component);
        componentDecorator.operation();


        ComponentDecorator componentDecorator1 = new ComponentDecoratorImpl1(new ComponentImpl());
        componentDecorator1.operation();
    }
}

