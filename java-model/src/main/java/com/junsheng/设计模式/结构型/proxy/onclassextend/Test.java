package com.junsheng.设计模式.结构型.proxy.onclassextend;

import com.junsheng.设计模式.结构型.proxy.Principal;
import com.junsheng.设计模式.结构型.proxy.User;

public class Test {
    public static void main(String[] args) {
        Principal user = new User();
        Principal user1 = new UserProxy();
        System.out.println(user.getUsername());
        System.out.println(user1.getUsername());
    }
}
