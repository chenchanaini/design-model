package com.junsheng.设计模式.结构型.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JdkProxy implements InvocationHandler {
    private Object target ;//需要代理的目标对象

    public JdkProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (Object.class.equals(method.getDeclaringClass())){//方法的声明是不是在Object
            return method.invoke(this,args);
        }
        System.out.println("JDK动态代理，监听开始！");
//        Object result = method.invoke(target, args);
//        System.out.println("JDK动态代理"+result);
        return null;
    }
//    //定义获取代理对象方法
//    private Object getJDKProxy(Object targetObject){
//        //为目标对象target赋值
//        this.target = targetObject;
//        //JDK动态代理只能针对实现了接口的类进行代理，newProxyInstance 函数所需参数就可看出
//        return Proxy.newProxyInstance(targetObject.getClass().getClassLoader(), targetObject.getClass().getInterfaces(), this);
//    }
}
