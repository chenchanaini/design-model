package com.junsheng.设计模式.结构型.proxy.classload;

import com.junsheng.设计模式.结构型.proxy.Principal;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

public class Proxy {
    public static Object  newProxyInstance(Object object) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String src="package com.junsheng.设计模式.结构型.proxy;\n" +
                "\n" +
                "import com.junsheng.设计模式.结构型.proxy.Principal;\n" +
                "\n" +
                "public class UserProxy implements Principal {\n" +
                "    private Principal principal;\n" +
                "\n" +
                "    public UserProxy(Principal principal) {\n" +
                "        this.principal = principal;\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public String getUsername() {\n" +
                "        String username = principal.getUsername();\n" +
                "        username =\"zhangsan\";\n" +
                "        return username;\n" +
                "    }\n" +
                "}";
        String fileName = System.getProperty("user.dir") + "/java-model/src/main/java/com/junsheng/proxy/UserProxy.java";
        File file = new File(fileName);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(src);
        fileWriter.flush();
        fileWriter.close();
        //compiler
        JavaCompiler systemJavaCompiler = ToolProvider.getSystemJavaCompiler();
        System.out.println(systemJavaCompiler.getClass());
        StandardJavaFileManager standardFileManager = systemJavaCompiler.getStandardFileManager(null, null, null);
        ArrayList<String> objects = new ArrayList<>();
        objects.add(src);
        Iterable javaFileObjects = standardFileManager.getJavaFileObjects(fileName);
        JavaCompiler.CompilationTask task = systemJavaCompiler.getTask(null, standardFileManager, null, null, null, javaFileObjects);
        task.call();
        standardFileManager.close();
        //load into memory
        URL[] urls = new URL[]{new URL("file:/"+System.getProperty("user.dir") + "/src")};
        URLClassLoader urlClassLoader = new URLClassLoader(urls);
        Class<?> aClass = urlClassLoader.loadClass("com.junsheng.设计模式.结构型.proxy.UserProxy");
        System.out.println(aClass);

        //实例化
        Constructor constructor = aClass.getConstructor(Principal.class);
        return constructor.newInstance(object);

    }
}
