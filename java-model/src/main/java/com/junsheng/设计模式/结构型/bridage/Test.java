package com.junsheng.设计模式.结构型.bridage;
// 桥接模式可以像桥一样承担两个任意抽象类和接口的实现联通
public class Test {
    public static void main(String[] args) {
        Abstraction abstraction = new AbstractionImpl(new ImplementorImpl());
        abstraction.operation();

        Abstraction abstraction1 = new AbstractionImpl(new ImplementorImpl1());
        abstraction1.operation();
    }
}
