package com.junsheng.设计模式.结构型.decorator;

public  class ComponentDecoratorImpl1 extends ComponentDecorator {

    public ComponentDecoratorImpl1(Component component) {
        super(component);
    }

    @Override
    public void operation(){
        super.operation();
        addedFunction();
    }

    private void addedFunction() {
        System.out.println("添加功能1");
    }
    ;
}
