package com.junsheng.设计模式.结构型.proxy.jdk;

import com.junsheng.设计模式.结构型.proxy.Principal;
import com.junsheng.设计模式.结构型.proxy.User;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Test {
    public static void main(String[] args) {
        Principal user = new User();
        Principal proxy =   (Principal)Proxy.newProxyInstance(Principal.class.getClassLoader(), new Class<?>[]{Principal.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("-------动态代理测试-----------");
                return null;
            }
        });

        //System.out.println(user);
        //动态代理类的类型不一致
        System.out.println(proxy);
        System.out.println(user.getClass());
        System.out.println(proxy.getClass());
        System.out.println(user.hashCode());
        System.out.println(proxy.hashCode());
        System.out.println(user==proxy);


//        ClassLoader.getSystemClassLoader().loadClass()

       // proxy.getUsername();
//        System.out.println(user.getUsername());
//        System.out.println(proxy.getUsername());
    }
}
