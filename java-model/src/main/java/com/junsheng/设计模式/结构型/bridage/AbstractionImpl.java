package com.junsheng.设计模式.结构型.bridage;

public  class AbstractionImpl extends Abstraction {
    public AbstractionImpl(Implementor implementor) {
        super(implementor);
    }

    @Override
    void operation() {
        implementor.operation();
        System.out.println("AbstractionImpl");
    }
}
