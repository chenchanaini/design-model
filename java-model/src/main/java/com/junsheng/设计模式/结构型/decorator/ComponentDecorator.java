package com.junsheng.设计模式.结构型.decorator;

public abstract class ComponentDecorator implements Component {
    private Component component;

    public ComponentDecorator(Component component) {
        this.component = component;
    }

    @Override
    public void operation(){
        component.operation();
    } ;
}
