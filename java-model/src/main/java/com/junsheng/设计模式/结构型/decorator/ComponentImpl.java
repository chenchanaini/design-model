package com.junsheng.设计模式.结构型.decorator;

public class ComponentImpl implements Component {
    @Override
    public void operation() {
        System.out.println("被装饰者行为");
    }
}
