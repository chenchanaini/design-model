package com.junsheng.设计模式.结构型.proxy;

public interface Principal {
    String getUsername();
}
