package com.junsheng;

import com.junsheng.core.MyAnnotationConfigApplicationContext;
import com.junsheng.core.test.SpringConfig;
import com.junsheng.core.test.springBean.User;

import java.lang.reflect.InvocationTargetException;

/**
 * Hello world!
 */
public class App 
{
    public static void main( String[] args ) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        MyAnnotationConfigApplicationContext applicationContext = new MyAnnotationConfigApplicationContext(SpringConfig.class);
        User bean = (User) applicationContext.getBean("user");
        User bean1 = applicationContext.getBean(User.class);
        System.out.println(bean);
        System.out.println(bean1);
    }
}
