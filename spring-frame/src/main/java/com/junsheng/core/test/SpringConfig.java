package com.junsheng.core.test;

import com.junsheng.core.annotation.ComponentScan;

@ComponentScan("com.junsheng.core.test.springBean")
public class SpringConfig {
}
