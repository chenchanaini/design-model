package com.junsheng.core.test.springBean;

import com.junsheng.core.annotation.Component;

@Component("user")
public class User {
    private String username;
    public User() {
    }
    public User(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
