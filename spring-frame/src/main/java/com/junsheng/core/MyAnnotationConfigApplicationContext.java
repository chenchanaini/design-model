package com.junsheng.core;

import com.junsheng.core.annotation.Component;
import com.junsheng.core.annotation.ComponentScan;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public class MyAnnotationConfigApplicationContext {
    private Class Config;
    private Map hashMap = new ConcurrentHashMap();
    public MyAnnotationConfigApplicationContext(Class config) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Config = config;
        //根据当前的类
        ComponentScan declaredAnnotation = (ComponentScan) config.getDeclaredAnnotation(ComponentScan.class);
        String value = declaredAnnotation.value();
        ClassLoader classLoader = config.getClassLoader();
        URL resource = classLoader.getResource(value.replaceAll("\\.","/"));
//        URL resource = classLoader.getResource("com/junsheng/core/test/springBean");
        String file = resource.getFile();
        File file1 = new File(file);
        if (file1.isDirectory()){
            File[] files = file1.listFiles();
            for(File file2:files){
                String absolutePath = file2.getAbsolutePath();
                if (!absolutePath.endsWith(".class")){
                    continue;
                }else{
//                    Properties properties = System.getProperties();
//                    Map<String, String> getenv = System.getenv();
                    String property = System.getProperty("java.class.path");
                    String classPath ="";
                  for(String s: property.split(";")) {
                      if (s.endsWith("classes")){
                          classPath =s;
                          break;
                      }
                  }
                    String s = absolutePath.substring(classPath.length());
                    String targetPath = s.replaceAll(".class", "");
                    String className = targetPath.replaceAll("\\\\",".");
                    Class<?> aClass = classLoader.loadClass(className);
                    if (aClass.isAnnotationPresent(Component.class)){
                        Component annotation = aClass.getAnnotation(Component.class);
                        System.out.println("进行springBean生成");
                        hashMap.put(annotation.value(),aClass.getConstructor().newInstance());
                    }


                }
                System.out.println(file2.getAbsolutePath());
            }
        }
    }
    public <T> T getBean(Class<T> objectclass){
        AtomicReference<T> Object = new AtomicReference<>();

        hashMap.forEach(
                (key,value)->{
                    if (value.getClass().isAssignableFrom(objectclass)){
                        Object.set(objectclass.cast(value));
                    }
                }
        );

        return Object.get();
    }

    public Object getBean(String beanName){
        return hashMap.get(beanName);
    }

}
