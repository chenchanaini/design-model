package cn.junsheng.行为型.责任链;

public class HandleRequest2 extends HandleRequest{
    @Override
    public void HandleRequest(String param) {
        if ("2".equals(param)){
            System.out.println("HandleRequest2 正在处理该请求");
        }else {
            this.getNext().HandleRequest(param);
        }
    }
}
