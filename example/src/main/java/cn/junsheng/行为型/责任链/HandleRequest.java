package cn.junsheng.行为型.责任链;

public abstract class HandleRequest {
    public HandleRequest getNext() {
        return next;
    }

    public void setNext(HandleRequest next) {
        this.next = next;
    }

    HandleRequest next;
    abstract void HandleRequest(String param);
}
